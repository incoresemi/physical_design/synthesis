
**⚠ WARNING: This project has been moved to [new location](https://gitlab.incoresemi.com/physical_design/synthesis.git). It will soon be archived and eventually deleted.**

# ASIC SYNTHESIS SCRIPTS

This repo contains various scripts related to Physical Design.

Directory structure:
- `genus`: contains the synthesis scripts using Cadence Genus
